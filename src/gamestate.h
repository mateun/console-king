#pragma once


struct InputState 
{
	int digiMoveH;
	int digiMoveV;
};


struct GameState {
	int builderTileX;
	int builderTileY;
	
	int gold;
	int wood;
	int stone;

	int goldFrameBefore;
	int woodFrameBefore;
	int stoneFrameBefore;
	
	int playerTurn;	
	
	int round;

	InputState input;

	
};
