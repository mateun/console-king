//
// Created by martin on 23.09.20.
//
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "terrain.h"
#include "gamestate.h"

SDL_Window * window;
SDL_Renderer * renderer;
SDL_GameController *controller = nullptr;
bool gameRunning = true;
Terrain * terrain;
GameState gameState;
SDL_Surface* hudSurface;
SDL_Texture* texWater;
SDL_Texture* goldText;
SDL_Texture* woodText;

void doFrame();
void initGame();

bool performanceloggingOn = false;
bool joystickLoggingOn = false;

TTF_Font* font;



void tempBlobInit() {
    SDL_Surface * surf = SDL_LoadBMP("../assets/water_tile.bmp");
    texWater = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);
}


void initGameControllers() {
	int nrOfMappings = SDL_GameControllerAddMappingsFromFile("mygcdb.txt");
	SDL_Log("mappings added: %d", nrOfMappings);
	SDL_Log("num joysticks: %d", SDL_NumJoysticks());
	if (SDL_NumJoysticks() == 0) {
		SDL_Log("Error, no gamecontroller found!");
		exit(1);
	}
	for (int i = 0; i < SDL_NumJoysticks(); i++) {
		if (SDL_IsGameController(i)) {
			controller = SDL_GameControllerOpen(i);	
			if (controller) {
				char *mapping;
				SDL_Log("Index \'%i\' is a compatible controller, named \'%s\'", i, SDL_GameControllerNameForIndex(i));
				mapping = SDL_GameControllerMapping(controller);
				SDL_Log("Controller %i is mapped as \"%s\".", i, mapping);
				SDL_free(mapping);
				break;
			} else {
				SDL_Log("Error; could not open gamecontroller %d. %s", i, SDL_GetError());
				exit(1);
			}

		}
	}
}

int main(int argc, char** args) {


    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER) < 0) {
        SDL_Log("error in init\n");
        exit(1);
    }

	window = SDL_CreateWindow("KING", 0, 0, 1920, 1080, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED 
									| SDL_RENDERER_PRESENTVSYNC
									);

	if (!window || !renderer) {
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "error on createion of window and renderer. %s\n", SDL_GetError());
		exit(1);
	}

	
	


	if (TTF_Init()==-1) 
	{
		SDL_Log("Error ini TTF init.%s", TTF_GetError());
		exit(1);
	}

	font = TTF_OpenFont("../assets/VCR_OSD_MONO.ttf", 26);
	if (!font) 
	{
		SDL_Log("error opening font! %s", TTF_GetError());
		exit(1);
	}


    SDL_SetWindowTitle(window, "King1005");
    initGame();
	tempBlobInit();

	hudSurface = SDL_CreateRGBSurfaceWithFormat(0, 1920, 300, 32, SDL_PIXELFORMAT_RGBA32);

    while (gameRunning) 
	{
		doFrame();
	}

	SDL_GameControllerClose(controller);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();


}


void moveBuilderDiscrete(GameState& gameState)
{
	if (gameState.input.digiMoveH == -1) {
		gameState.builderTileX -= 1;
	}	
	if (gameState.input.digiMoveH == 1)  {
		gameState.builderTileX += 1;
	}


	if (gameState.input.digiMoveV == -1) {
		gameState.builderTileY -= 1;
	}	
	if (gameState.input.digiMoveV == 1)  {
		gameState.builderTileY += 1;
	}
	SDL_Log("builder tileX %d", gameState.builderTileX);
}

void renderBuilder(SDL_Renderer* renderer, GameState& gameState)
{

	SDL_Rect dr;
	dr.x = gameState.builderTileX * 32;
	dr.y = gameState.builderTileY * 32;
	dr.w = 32;
	dr.h = 32;
	SDL_RenderCopy(renderer, texWater, nullptr, &dr);
}

/*void moveBuilderAnalog() {
	
	blobMoveX += digiMoveH * 3.0f;
	if (digiMoveH == 0 ) {
		blobMoveX -= (int) blobMoveX % 16;
	}
	builderTileX = blobMoveX / 32;

	blobMoveY += digiMoveV * 3.0f;
	if (digiMoveV == 0 ) {
		blobMoveY-= (int) blobMoveY % 16;
	}
	builderTileY = blobMoveY / 32;
}

void getControllerInput() {
	// Analog input
	Sint16 hAxis = SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTX);
	if (abs(hAxis) > 3500) {
		if (hAxis < -3500) {
			 digiMoveH = -1;
		}
		if (hAxis > 2300) {
			digiMoveH = 1;

		}
	} else {
		digiMoveH = 0;
	}
	
	Sint16 vAxis = SDL_GameControllerGetAxis(controller, SDL_CONTROLLER_AXIS_LEFTY);
	if (abs(vAxis) > 3500) {
		if (vAxis < -3500) {
			 digiMoveV = -1;
		}
		if (vAxis > 2300) {
			digiMoveV = 1;

		}
	} else {
		digiMoveV = 0;
	}
	if (joystickLoggingOn) {
		SDL_Log("Axis input: %d/%d", hAxis, vAxis);
	}
}
*/




void renderText(SDL_Renderer* renderer, const std::string text, int posX, int posY) 
{
	SDL_Color color = {240, 220, 220, 225};
	SDL_Surface *textSurface;
	if (!(textSurface = TTF_RenderText_Solid(font, text.c_str(), color )))
	{
		SDL_Log("Error rendering text! %s", TTF_GetError());
		exit(1);
	}
	else 
	{
		SDL_Texture* t = SDL_CreateTextureFromSurface(renderer, textSurface);	
		SDL_Rect dr;
		dr.x = posX;
		dr.y = posY;
		dr.w = textSurface->w;
		dr.h = textSurface->h;
		SDL_RenderCopy(renderer, t, nullptr, &dr);
		SDL_FreeSurface(textSurface);
		SDL_DestroyTexture(t);
	}

}


void renderTextEff(SDL_Renderer* renderer, const std::string text, int posX, int posY) {
	SDL_Color color = {240, 220, 220, 225};
	SDL_Surface *textSurface;
	if (!(textSurface = TTF_RenderText_Solid(font, text.c_str(), color )))
	{
		SDL_Log("Error rendering text! %s", TTF_GetError());
		exit(1);
	}
	else 
	{
		SDL_Rect dr;
		dr.x = posX;
		dr.y = posY;
		dr.w = textSurface->w;
		dr.h = textSurface->h;

		SDL_FillRect(hudSurface, &dr, SDL_MapRGB(hudSurface->format, 0, 0, 0));
		SDL_BlitSurface(textSurface, NULL, hudSurface, &dr);
		SDL_FreeSurface(textSurface);
	}


	//renderText(renderer, txtGold, 10, 10);

	//renderText(renderer, txtWood, 10, 50);

}

void renderHudTexts(SDL_Renderer* renderer) {
	std::string txtGold = "Gold: " + std::to_string(gameState.gold);
	renderTextEff(renderer, txtGold, 10, 10);
	std::string txtWood = "Wood: " + std::to_string(gameState.wood);
	renderTextEff(renderer, txtWood, 10, 50);

	SDL_Texture* t = SDL_CreateTextureFromSurface(renderer, hudSurface);	
	SDL_Rect dr;
	dr.x = 0;
	dr.y = 500;
	dr.w = hudSurface->w;
	dr.h = hudSurface->h;
	SDL_RenderCopy(renderer, t, nullptr, &dr);
	SDL_DestroyTexture(t);

}

Uint64 performanceFrequency = SDL_GetPerformanceFrequency();

int frameCount = 0;


void doFrame() {
	frameCount++;
	Uint64 start = SDL_GetPerformanceCounter();


	// We need to reset the inputs 
	gameState.input.digiMoveH = 0;
	gameState.input.digiMoveV = 0;
	

	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			gameRunning = false;
		}
		if (event.type== SDL_KEYDOWN) {
			if (event.key.keysym.sym == SDLK_ESCAPE) {
				gameRunning = false;
			}	
			if (event.key.keysym.sym == SDLK_l) {
				performanceloggingOn = !performanceloggingOn;			
				SDL_Log("performance logging now: %d", performanceloggingOn);
			}
			if (event.key.keysym.sym == SDLK_j) {
				joystickLoggingOn = !joystickLoggingOn;
				SDL_Log("joystick logging now: %d", joystickLoggingOn);
			}
			
		}
		if (event.type == SDL_CONTROLLERBUTTONDOWN) 
		{
			SDL_Log("joystick button down!");
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT)
			{
				SDL_Log("joystick button left!");
				gameState.input.digiMoveH = -1;
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT)
			{
				SDL_Log("joystick button right!");
				gameState.input.digiMoveH = 1;
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_UP)
			{
				gameState.input.digiMoveV = -1;
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN)
			{
				gameState.input.digiMoveV = 1;
			}
		
		}
	}

	moveBuilderDiscrete(gameState);

	SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
	SDL_RenderClear(renderer);

	//renderTerrainComplete(renderer, terrain);
	renderBuilder(renderer, gameState);

	renderHudTexts(renderer);

	Uint64 end= SDL_GetPerformanceCounter();	
	double perfDiff = (double) ((end - start) * 1000 * 1000) / performanceFrequency;

	SDL_RenderPresent(renderer);

	Uint64 end2= SDL_GetPerformanceCounter();	
	double perfDiff2 = (double) ((end2 - start) * 1000 * 1000) / performanceFrequency;
	if (performanceloggingOn) {
		SDL_Log("ticksperf: %f %f", perfDiff, perfDiff2);
	}
}

void initGame() {
	gameState.builderTileX = 10;
	gameState.builderTileY = 10;
	gameState.gold = 100;
	gameState.wood = 300;
	gameState.round = 0;

	initGameControllers();
    initTerrainTextures(renderer);
    terrain = createTerrain(renderer, 60, 30);
}

