//
// Created by martin on 23.09.20.
//

#include <SDL2/SDL_log.h>
#include "terrain.h"
#include <string>

SDL_Texture** terrainTiles;
SDL_Texture* terrainTextureComplete;

void checkSurf(SDL_Surface* surf) {
    if (!surf) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "could not load texture. %s", SDL_GetError());
        exit(1);
    }
}

void initTerrainTextures(SDL_Renderer* renderer) {
    terrainTiles = new SDL_Texture*[4];
    SDL_Surface * surf = SDL_LoadBMP("../assets/grass_tile.bmp");
    checkSurf(surf);
    terrainTiles[0] = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);

    surf = SDL_LoadBMP("../assets/sand_tile.bmp");
    checkSurf(surf);
    terrainTiles[1]= SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);

    surf = SDL_LoadBMP("../assets/stone_tile.bmp");
    checkSurf(surf);
    terrainTiles[2] = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);

    surf = SDL_LoadBMP("../assets/water_tile.bmp");
    checkSurf(surf);
    terrainTiles[3] = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_FreeSurface(surf);

	terrainTextureComplete = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, 
				SDL_TEXTUREACCESS_TARGET, 1920, 1080);

	if (!terrainTextureComplete) {
		SDL_Log("error creating overall terrain texture. exiting");
		exit(1);
	}
}

Terrain *createTerrain(SDL_Renderer* renderer, int sizeX, int sizeY) {

	SDL_Log("Creating the terrain");
	
    Terrain* terrain = new Terrain();
    terrain->sizeX = sizeX;
    terrain->sizeY = sizeY;
    terrain->tiles = new TerrainType[sizeX * sizeY];
    for (int i=0; i<sizeX*sizeY;i++) {
        terrain->tiles[i] = TerrainType::GRASS;
    }

    terrain->tiles[10] = TerrainType::SAND;
    terrain->tiles[11] = TerrainType::STONE;
    terrain->tiles[12] = TerrainType::WATER;

	terrain->terrainTextureComplete = terrainTextureComplete;

	SDL_SetRenderTarget(renderer, terrain->terrainTextureComplete);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
	renderTerrainPerTile(renderer, terrain);
	SDL_SetRenderTarget(renderer, nullptr);

	

    return terrain;
}

void renderTerrainPerTile(SDL_Renderer *renderer, Terrain *terrain) {
    for (int x=0; x<terrain->sizeX;x++) {
        for (int y=0; y<terrain->sizeY;y++) {
            auto terrainType = terrain->tiles[y * terrain->sizeX + x];
            SDL_Rect dr;
            dr.w = 32;
            dr.h = 32;
            dr.x = x * 32;
            dr.y = y * 32;
            SDL_RenderCopy(renderer, terrainTiles[terrainType], nullptr, &dr);
        }
    }
}

void renderTerrainComplete(SDL_Renderer* renderer, Terrain* terrain) {
	SDL_Rect dr;
	dr.x = 200; 
	dr.y = 200;
	dr.w = 600;
	dr.h = 400;
	SDL_RenderCopy(renderer, terrain->terrainTextureComplete, nullptr, nullptr);

}
