//
// Created by martin on 23.09.20.
//

#ifndef KING1005_TERRAIN_H
#define KING1005_TERRAIN_H

#include <SDL2/SDL_render.h>

enum TerrainType {
    GRASS,
    SAND,
    STONE,
    WATER
};

struct Terrain {
   TerrainType* tiles;
   int sizeX;
   int sizeY;
	SDL_Texture* terrainTextureComplete;

};

Terrain* createTerrain(SDL_Renderer*, int sizeX, int sizeY);
void renderTerrainPerTile(SDL_Renderer* renderer, Terrain* terrain);
void renderTerrainComplete(SDL_Renderer* renderer, Terrain* terrain);
void initTerrainTextures(SDL_Renderer* renderer);

#endif //KING1005_TERRAIN_H
